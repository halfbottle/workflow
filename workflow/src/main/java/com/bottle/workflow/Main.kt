package com.bottle.workflow

/**
 * 测试代码
 */
fun main(args: Array<String>) {
    var flag = false
    if (flag) {
        workflow()
    } else {
        procedure()
    }
}

private fun procedure() {
    val taskA = Task("taskA") {
        val msg = it.inputParam["start"] as? String
        msg?.let { println("input message: $msg") }
        sleep(100)
        it.progress(10, "")
        sleep(100)
        it.progress(50, "")
        sleep(100)
        it.progress(90, "")
        sleep(100)
        it.complete("Hello, this message is from taskA")
    }
    val taskB = Task("taskB") {
        val msg = it.inputParam[taskA.name] as? String
        msg?.let { println("input message: $msg") }
        sleep(100)
        it.progress(10, "")
        sleep(100)
        it.progress(50, "")
        sleep(100)
        it.progress(90, "")
        sleep(100)
        it.complete()
    }
    val taskC = Task("taskC") {
        val msg = it.inputParam[taskA.name] as? String
        msg?.let { println("input message: $msg") }
        sleep(100)
        it.progress(10, "")
        sleep(100)
        it.progress(50, "")
        sleep(100)
        it.complete("Hello, this message is from taskC")
    }
    val taskD = Task("taskD") {
        val msg = it.inputParam[taskC.name] as? String
        msg?.let { println("input message: $msg") }
        sleep(100)
        it.progress(10, "")
        sleep(100)
        it.progress(50, "")
        sleep(100)
        it.complete()
    }
    val taskE = Task("taskE") {
        if (!it.canceled) {
            sleep(100)
            it.progress(10, "")
            sleep(100)
            it.progress(50, "")
            sleep(100)
            it.complete()
        }
    }
    val taskF = Task("taskF") {
        if (!it.canceled) {
            sleep(100)
            it.progress(10, "")
            sleep(100)
            it.progress(50, "")
            sleep(100)
            it.complete()
        }
    }
    val procedure = Procedure()
    procedure.apply {
        addTask(taskA)
        addTask(taskA, taskB)
        addTask(taskA, taskC)
        addTask(arrayOf(taskB, taskC), taskD)
        addTask(taskB, taskE)
        addTask(taskD, taskF)
    }.start(object : OnProcedureListener {

        override fun onCompleted() {
            println("Procedure is Completed")
        }

        override fun onFailed(task: Task) {
            println("Procedure is Failed")
        }

    }, mutableMapOf("start" to "Hello, this message is from Procedure"))
}

private fun workflow() {
    val taskA = Task("taskA") {
        sleep(100)
        it.progress(10, "")
        sleep(100)
        it.progress(50, "")
        sleep(100)
        it.complete()
    }
    val taskB = Task("taskB") {
        sleep(100)
        it.progress(10, "")
        sleep(100)
        it.progress(50, "")
        sleep(100)
        it.complete()
    }
    val taskC = Task("taskC") {
        if (!it.canceled) {
            sleep(100)
            it.progress(10, "")
            sleep(100)
            it.progress(50, "")
            sleep(100)
            it.complete()
        }
    }
    val taskD = Task("taskD") {
        sleep(100)
        it.progress(10, "")
        sleep(100)
        it.progress(50, "")
        sleep(100)
        it.complete()
    }
    val taskE = Task("taskE") {
        if (!it.canceled) {
            sleep(100)
            it.progress(10, "")
            sleep(100)
            it.progress(50, "")
            sleep(100)
            it.complete()
        }
    }
    val workFlow = WorkFlow()
    workFlow.addProcedure(Procedure().addTask(taskA).addTask(taskB))
    workFlow.addProcedure(Procedure().addTask(taskC).addTask(taskD))
    workFlow.addProcedure(Procedure().addTask(taskE))
    workFlow.start(object : OnProcedureListener {

        override fun onCompleted() {
            println("WorkFlow is Completed")
        }

        override fun onFailed(task: Task) {
            println("WorkFlow is Completed")
        }

    })
}

private fun sleep(time: Long) {
    try {
        Thread.sleep(time)
    } catch (e: Exception) {
        e.printStackTrace()
    }

}
